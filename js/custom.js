$(document).ready(function () {
    $(".travelinfo-block").on("click", function () {
        $(".travelinfo-block").removeClass("active");
        $(this).addClass("active");
        $(this).siblings().find('input[type=radio]').prop('checked', false);
        $(this).find('input[type=radio]').prop('checked', true);
    });

    //Slider 
    $('#tripoffers').owlCarousel({
        loop: false,
        margin: 0,
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

    //Slider width video
    $('#slide-video .owl-carousel').owlCarousel({
        loop: false,
        margin: 20,
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 2
            }
        }
    });

    // dynamic modal video
    $('.videoplay').on('click', function() {
        var videoUrl = $(this).data('vidsrc');
        $('#videoboxmodal').find('iframe').attr('src', videoUrl);
        $('#videoboxmodal').modal('show');  
        console.log(videoUrl);      
    });

    $('#videoboxmodal').on('hidden.bs.modal', function() {
        $('#video-modalbox iframe').attr('src', null);
    });

    $("#videoboxmodal .close").on("click", function(){
        $('#videoboxmodal').find('iframe').attr('src', null);       
    });
    

});